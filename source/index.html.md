---
title: Penguicorp - the Board of Directors of Penguicon
---

#The Penguicon Board

## An archive of year-over-year information

![Not a photo of Penguicorp](/images/penguicorp.png)

- How are the Penguicon Board and Convention Committee different?

    Because the convention Penguicon is incorporated, by law it must have a Board of Directors. The Board of Directors is not the same thing as the Convention Committee (or Concom). If you want to talk about how this year's Penguicon should be run, talk to the Conchair and Concom, not the Board. Here is a list of what is involved in the Board, to clarify the difference between Board and Concom.

    The Board elects a Convention Chair (or Conchair), and gives that person total independent authority to put on one Penguicon convention in one year without interference from the Board. The Board is committed to not push the Conchair around. The only way the Board would interfere would be in a crisis sufficient to justify complete removal of the Conchair.

    - The Board receives a budget submitted by the Conchair, and approves or denies the total dollar figure requested for that year's convention based on the Conchair's justifications. But the Board does not micromanage the line items in the budget during the approval process.
    - Because the Conchair is only authorized to control one year of Penguicon, the Board handles multi-year financial commitments such as web host, P.O. Box, and storage cube.
    - The Board has four meetings per year to accomplish the above business: the **second Saturday in January, March, June and September** at 12:00 P.M. (Noon)
    - The Board has the ability to change Penguicon's Bylaws of incorporation by vote.

    By contrast, here is how the Concom is different in each of those points.

    - The Concom is an organization assembled by a Convention Chair (or Conchair) to create one Penguicon convention in one particular year.
    - The Convention Chair may change line items in the Board-approved budget any way they wish, to fit within the approved total dollar figure.
    - Any decision that fits into one year falls to the authority of the Concom of that year.
    - The Concom normally meets about once per month, and plans this year's convention in their meetings.
    - The Concom is not normally a voting body.


- How can I contact the Board?
    - Again, if you wish to talk about how a specific year is run, contact the Conchair instead. But to discuss year-over-year decisions, you can contact the Board directors privately at: ![an image of the board-members-only email address, in order to deter spam bots](/images/board-members-only-email.png)

- What else has Penguicorp done besides Penguicon?

- [The Board Directors and Officers of the Board](/penguiconboard/boardandofficers)

- Board Meeting Calendar and Agendas
    - The [Bylaws](/penguiconboard/bylaws) mandate that the Board will meet, at a minimum, on the second Saturday in January, March, June and September.

- Board Meeting Minutes

- Media Kit

- Documents
    - [Bylaws](/penguiconboard/bylaws) ... includes amendments up to and including the September 2017 Board meeting

- Buy swag and T-shirts

- Join or leave email lists

- [Public archives of slides/presentations/videos etc](http://presentations.penguicon.org/)

- [Photo galleries](https://photos.penguicon.org/)

- Source code repositories which are connected to Penguicon/Penguicorp
    - [This site on GitLab](https://gitlab.com/NemoRathwald/penguiconboard)
    - [The Penguicon Github repositories](https://github.com/Penguicon)

- Sites for present and past years of Penguicon
    - [Penguicon 2021](https://2021.penguicon.org)
    - [Penguicon 2020](https://2020.penguicon.org)
    - [Penguicon 2019](https://2019.penguicon.org)
    - [Penguicon 2018](https://2018.penguicon.org)
    - [Penguicon 2017](https://2017.penguicon.org)
    - [Penguicon 2016](https://2016.penguicon.org)
    - Penguicon 2015
    - [Penguicon 2014](https://2014.penguicon.org)
    - Penguicon 2013
    - [Penguicon 2012](https://2012.penguicon.org)
    - [Penguicon 2011](https://pc9.penguicon.org)
    - [Penguicon 2010](https://pc8.penguicon.org)
    - Penguicon 2009
    - [Penguicon 2008](https://pc6.penguicon.org)
    - [Penguicon 2007](https://pc5.penguicon.org)
    - [Penguicon 2006](https://pc4.penguicon.org)
    - [Penguicon 2005](https://pc3.penguicon.org)
    - [Penguicon 2.0 in 2004](https://pc2.penguicon.org)
    - [The first Penguicon, 2003](https://pc1.penguicon.org)
