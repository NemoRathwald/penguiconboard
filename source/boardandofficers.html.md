---
title: Penguicon Board Directors And Officers
---

# List of Board Directors

According to the [Bylaws](/penguicon/bylaws), Article 3, Section 1, there are to be seven Board Directors.

As of 01/26/2021, the Penguicon Board of Directors are:

- Randy Bradakis (President) since June 2010

- Amanda Long-Adams (Treasurer) since April 2012

- James Hice since April 2013

- Chelle Silas since March 2019

- Jessica Roland since April 2019

- vacancy

- vacancy

The former Conchairs on the Board are Jessica Roland and Randy Bradakis.

The Conchair is requested to attend Board meetings, but the rest of the Concom is not required. The interested public is welcome to attend Board meetings.

Contact Board Members Privately: ![an image of the board-members-only email address, in order to deter spam bots](/images/board-members-only-email.png)

## Former Board Members:

- Matt Arnold (Vice-President) May 2010 to May 2020

- Steve Gutterman (Secretary) 2002 to February 2019

- Jer Lance September 2011 to February 2019

- Gini Judd June 2009 to January 2019

- Mark Szlaga, June 2010 to September 2012

- Aaron Thul, 1st term began May 2005; 2nd term May 2010 to April 2012

- Chuck Child, 2007 to June 2011

- Jordan Malokofsky until June 2010

- Angela Pineo June 2009 to June 2010

- Garrett Kajmowicz, 2006 to 2009

- John Guest, 2002 to 2006

- Patricia Altergott, 2004 to 2006

- Susan Harris, 2002 to 2006

- Rob Landley, 2002 to 2006

- Tracy Worcester, Treasurer, 2002 to 2006 2007 to 2009

# List Of Board Officers

- Paul Keyes, Information Technology Officer
