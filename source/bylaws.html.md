---
title: Nonprofit Bylaws of Penguicon
---

- [**Home**](/penguiconboard)

The following document reflects bylaw amendments up to and including the September 2017 Board meeting.

# Bylaws of PenguiCon

## Article 1

## Offices

### Section 1. Principal Office

The principal office of the corporation is located in Oakland County, State of Michigan.

### Section 2. Change of Address

The designation of the county or state of the corporation's principal office may be changed by amendment of these Bylaws. The Board of Directors may change the principal office from one location to another within the named county by noting the changed address and effective date below, and such changes of address shall not be deemed, nor require, an amendment of these Bylaws:

New Address:
________________________________________
Dated: ________, 20__

New Address:
________________________________________
Dated: ________, 20__

New Address:
________________________________________
Dated: ________, 20__

## Article 2

## Nonprofit Purposes

### Section 1. Specific Objectives and Purposes

The specific objectives and purposes of this corporation shall be:

- a. to educate the public concerning the nature of open source and SF (speculative fiction) thought, development, and issues using a variety of
forums, both online and face-to-face;

- b. to sponsor and run one or more annual conventions with guests of interest to the open SF and open source communities where community members and guests can gather to network and exchange ideas;

- c. to sponsor special events open to the general public where ideas, opinions, and writings relating to open source and SF related issues may be expressed and shared with others;

- d. to redefine our education program from time to time to meet the changing needs of the SF and open source communities;

- e. be guided in principle by the motto "to do cool stuff".

## Article 3

## Directors

### Section 1. Number

The corporation shall have seven directors and collectively they shall be known as the Board of Directors.

### Section 2. Qualifications

Directors shall be of the age of majority in this state, and shall not have been convicted of any financially-related felony.

### Section 3. Powers

Subject to the provisions of the laws of this state and any limitations in the Articles of Incorporation and these Bylaws relating to action required or permitted, the activities and affairs of this corporation shall be conducted and all corporate powers shall be exercised by or under the direction of the Board of Directors.

###Section 4. Duties

It shall be the duty of the directors to:

- a. Perform any and all duties imposed on them collectively or individually by law, by the Articles of Incorporation, or by these Bylaws;

- b. Appoint and remove, employ and discharge, and, except as otherwise provided in these Bylaws, prescribe the duties and fix the compensation, if any, of all officers, agents and employees of the corporation;

- c. Supervise all officers, agents and employees of the corporation to assure that their duties are performed properly;

- d. Meet at such times and places as required by these Bylaws;

- e. Register their addresses with the Secretary of the corporation, and notices of meetings mailed to them at such addresses shall be valid notices thereof.

### Section 5. Term of Office

Each director shall hold office until resignation or removal from their office.

### Section 6. Place Of Meetings

Place Of Meetings Meetings shall be held at the principal office of the corporation or at a location chosen at the discretion of the President or through a majority vote of the Board of Directors. If the President and a majority vote of the Board of Directors are in conflict, the vote of the Board will prevail.


### Section 7. Regular Meetings

Regular meetings of Directors shall be held on the second Saturday in January, March, June and September at 12:00 P.M., unless such day falls on a legal holiday, in which event the regular meeting shall be held at the same hour and place on the next Saturday. A different date may be selected by the President provided that all directors are notified at least 45 days in advance in the manner used for Special Meetings.


### Section 8.

[REMOVED]

### Section 9. Special Meetings

Special meetings of the Board of Directors may be called by the President, the Vice President, the Secretary, by any two directors, or, if different, by the persons specifically authorized under the laws of this state to call special meetings of the board. Such meetings shall be held at the principal office of the corporation or, if different, at the place designated by the person or persons calling the special meeting.


### Section 10. Notice of Meetings

Unless otherwise provided by the Articles of Incorporation, these Bylaws, or provisions of law, the following provisions shall govern the giving of notice for meetings of the Board of Directors:

- a. Regular Meetings. No notice need be given of any regular meeting of the board of directors.

- b. Special Meetings. At least one week prior notice shall be given by the Secretary of the corporation to each director of each special meeting of the board. Such notice may be oral or written, may be given personally or impersonally. In the case of impersonal notification where the Secretary shall not be able to verify receipt, the director to be contacted shall acknowledge personal receipt of notice by a return message or telephone call within twenty-four hours of reception.

- c. Waiver of Notice. Whenever any notice of a meeting is required to be given to any director of this corporation under provisions of the Articles ofIncorporation, these Bylaws or the law of this state, a waiver of notice in writing signed by the director, whether before or after the time of the meeting, shall be equivalent to the giving of such notice.

- d. Emergency Meetings. A meeting of the board of directors may be called without any advanced notice. An emergency meeting will require an affirmative vote by a majority of the current serving board of directors to pass any motion. A record of the minutes of the meeting must be made public within 72 hours of the meeting. All motions approved by an emergency meeting shall expire at the end of the next Regular or Special meeting unless approved at such meeting.

### Section 11. Quorum for Meetings

A quorum shall consist of a majority of the members of the Board of Directors. Except as otherwise provided under the Articles of Incorporation, these Bylaws or provisions of law, no business shall be considered by the board at any meeting at which the required quorum is not present, and the only motion which the Chair shall entertain at such meeting is a motion to adjourn.

### Section 12. Majority Action As Board Action

Every act or decision done or made by a majority of the directors present at a meeting duly held at which a quorum is present is the act of the Board of Directors, unless the Articles of Incorporation, these Bylaws, or provisions of law require a greater percentage or different voting rules for approval of a matter by the board.

### Section 13. Conduct of Meetings

Meetings of the Board of Directors shall be presided over by the Chairperson of the Board, or in the absence of the Chairperson of the board, by a Chairperson chosen by a majority of the directors present at the meeting. The Secretary of the corporation shall act as secretary of all meetings of the board, provided that, in his or her absence, the presiding officer shall appoint another person to act as Secretary of the Meeting.

Meetings shall be governed by such procedures as may be approved from time to time by the board of directors, insofar as such rules are not inconsistent with or in conflict with the Articles of Incorporation, these Bylaws or with provisions of law.

### Section 14. Vacancies

Vacancies on the Board of Directors shall exist (1) on the death, resignation or removal of any director, and (2) whenever the number of authorized directors is increased.

Any director may resign effective upon giving written notice to the President, the Secretary or the Board of Directors, unless the notice specifies a later time for the effectiveness of such resignation. No director may resign if the corporation would then be left without a duly elected director or directors in charge of its affairs, except upon notice to the Office of the Attorney General or other appropriate agency of this state.

Directors may be removed from office, with or without cause, as permitted by and in accordance with the laws of this state.

Unless otherwise prohibited by the Articles of Incorporation, these Bylaws or provisions of law, vacancies on the board may be filled by approval of the Board of Directors. If the number of directors then in office is less than a quorum, a vacancy on the board may be filled by approval of a majority of the directors then in office or by a sole remaining director. A person elected to fill a vacancy on the board shall hold office until the next election of the Board of Directors or until his or her death, resignation or removal from office.

###Section 15. Nonliability of Directors

The directors shall not be personally liable for the debts, liabilities or other obligations of the corporation.

###Section 16. Indemnification by Corporation of Directors and Officers

The directors and officers of the corporation shall be indemnified by the corporation to the fullest extent permissible under the laws of this state.

### Section 17. Insurance For Corporate Agents

Except as may be otherwise provided under provisions of law, the Board of Directors may adopt a resolution authorizing the purchase and maintenance of insurance on behalf of any agent of the corporation (including a director, officer, employee or other agent of the corporation) against liabilities asserted against or incurred by the agent in such capacity or arising out of the agent's status as such, whether or not the corporation would have the power to indemnify the agent against such liability under the Articles of Incorporation, these Bylaws or provisions of law.

### Section 18. Mandatory Participation

A Director shall be considered to have resigned from office if they do any of the following:

1. If a Director fails to attend 3 consecutive regularly scheduled board meetings.
2. If a Director does not participate in Penguicon activities for a period of 6 or more months.

For the purposes of this section, Penguicon activities shall include:

- A. Penguicon sponsored conventions
- B. Penguicon board meetings
- C. Penguicon convention committee meetings

## Article 4

## Permanent Officers

### Section 1. Designation Of Officers

The permanent officers of the corporation shall consist of be a President, a Vice President, a Secretary and a Treasurer. The corporation may also have other titled officers as may be determined from time to time by the Board of Directors.

### Section 2. Qualifications

Any person may serve as officer of this corporation provided they have the qualifications for director as listed in Article 3 Section 2. Officers may be, but are not required to be members of Board of Directors.

### Section 3. Election and Term of Office

Officers shall be elected by the Board of Directors, at any time, and each officer shall hold office until he or she resigns or is removed or is otherwise disqualified to serve, or until his or her successor shall be elected and qualified, whichever occurs first.

### Section 4. Removal and Resignation

Any officer may be removed, either with or without cause, by a majority vote of the full Board of Directors, at any time. Any officer may resign at any time by giving written notice to the Board of Directors or to the President or Secretary of the corporation. Any such resignation shall take effect at the date of receipt of such notice or at any later date specified therein, and, unless otherwise specified therein, the acceptance of such resignation shall not be necessary to make it effective. The above provisions of this Section shall be superseded by any conflicting terms of a contract which has been approved or ratified by the Board of Directors relating to the employment of any officer of the corporation.

### Section 5. [MOVED TO ARTICLE 3, SECTION 18]


### Section 6. Duties of President

The President shall be the chief executive officer of the corporation and shall, subject to the control of the Board of Directors, supervise and control the affairs of the corporation and the activities of the officers.

The President shall preside at all meetings of the Board of Directors. Except as otherwise expressly provided by law, by the Articles of Incorporation or by these Bylaws, he or she shall, in the name of the corporation, execute such deeds, mortgages, bonds, contracts, checks or other instruments which may from time to time be authorized by the Board of Directors.


### Section 7. Duties of Vice President

In the absence of the President, or in the event of his or her inability or refusal to act, the Vice President shall perform all the duties of the President, and when so acting shall have all the powers of, and be subject to all the restrictions on, the President. The Vice President shall have other powers and perform such other duties as may be prescribed by law, by the Articles of Incorporation or by these Bylaws or as may be prescribed by the Board of Directors.

### Section 8. Duties of Secretary

The Secretary shall:

Certify and keep at the principal office of the corporation the original, or a copy, of these Bylaws as amended or otherwise altered to date.

Keep at the principal office of the corporation or at such other place as the board may determine, a book of minutes of all meetings of the directors, and, if applicable, meetings of committees of directors, recording therein the time and place of holding, whether regular or special, how called, how notice thereof was given, the names of those present or represented at the meeting and the proceedings thereof.

See that all notices are duly given in accordance with the provisions of these Bylaws or as required by law. Be custodian of the records and of the seal of the corporation and affix the seal, as authorized by law or the provisions of these Bylaws, to duly executed documents of the corporation.

Exhibit at all reasonable times to any director of the corporation, or to his or her agent or attorney, on request therefore, the Bylaws, and the minutes of the proceedings of the directors of the corporation.

In general, perform all duties incident to the office of Secretary and such other duties as may be required by law, by the Articles of Incorporation or by these Bylaws or which may be assigned to him or her from time to time by the Board of Directors.

### Section 9. Duties Of Treasurer

The Treasurer shall:

Have charge and custody of, and be responsible for, all funds and securities of the corporation, and deposit all such funds in the name of the corporation in such banks, trust companies or other depositories as shall be selected by the Board of Directors.

Receive, and give receipt for, monies due and payable to the corporation from any source whatsoever.

Disburse, or cause to be disbursed, the funds of the corporation as may be directed by the Board of Directors, taking proper vouchers for such
disbursements.

Keep and maintain adequate and correct accounts of the corporation's properties and business transactions, including accounts of its assets, liabilities, receipts, disbursements, gains and losses.

Exhibit at all reasonable times the books of account and financial records to any director of the corporation, or to his or her agent or attorney, on request therefore.

Render to the President and directors, whenever requested, an account of any or all of his or her transactions as Treasurer and of the financial condition of the corporation.

Prepare, or cause to be prepared, and certify, or cause to be certified, the financial statements to be included in any required reports.

In general, perform all duties incident to the office of Treasurer and such other duties as may be required by law, by the Articles of Incorporation of the corporation or by these Bylaws or which may be assigned to him or her from time to time by the Board of Directors.

### Section 10. Compensation

The salaries of the officers, if any, shall be fixed from time to time by resolution of the Board of Directors.

## Article 5

## Convention Committee

### Section 1. Purpose.

The Convention Committee exists to provide efficient implementation of the goals and objectives of the corporation, specifically, the holding of an annual convention.

### Section 2. Convention Chair.

All powers of the Convention Committee will be vested in the Convention Chair (hereafter referred to as the ConChair), and lesser committee members as may be appointed and delegated by the ConChair. The ConChair will be elected by a majority vote of the Board of Directors. The ConChair's term ends at the end of the first regular Board meeting following the convention under that ConChair’s authority. The ConChair will be responsible for producing their designated PenguiCon convention during their term. The ConChair will be considered an officer of the corporation for the duration of their term.

### Section 3. Budget.

The ConChair is required to present to the Board for approval a budget for the convention under their authority at a board meeting to be held no later than six months prior to the convention under their authority.

### Section 4. Finances.

The ConChair will provide, or cause to be provided, to the Treasurer a current general ledger of convention finances. This ledger must be provided to the Treasurer no later than the 3 days prior to each of the scheduled board meetings.

### Section 5. Authority.

The ConChair, or anyone designated in writing by the ConChair, are the only individuals authorized to enter into any contract or execute and deliver any instrument or payments in the name of, and on behalf of the corporation for producing the PenguiCon convention to occur during that ConChair’s term.

###Section 6. Limitations of Authority.

The ConChair may make no agreements or contract resulting either in spending beyond that approved in the convention budget or resulting in contractual obligations which extend past the end of the ConChair's term of office without board consent and written approval.

### Section 7. Limitations on Delegation.

(This section intentionally left blank.)

### Section 8. Number of ConChairs

The Board may authorize more than one ConChair at any given time, providing that each ConChair is given authority over a different convention.

## Article 6

## Project Managers

### Section 1. Purpose.

Project managers exist to provide efficient implementation of the goals and objectives of the corporation through the management of a one-time event (Project).

### Section 2. Selection.

A Project Manager shall be selected by a majority vote of the Board of Directors. The criteria for being a Project Manager shall be the same as being a Permanent Officer of the corporation. The Project manager shall maintain their role until the completion or termination of the project, their resignation, or removal by a majority vote of the Board of Directors.

### Section 3. Budget.

A Project Manager must present a budget to the Board of Directors for approval prior to enacting the project. This budget may be amended by the Board of Directors at any time.

### Section 4. Finances

The Project Manager will provide, or cause to be provided, to the Treasurer a current general ledger of project finances upon demand, and upon the completion of the project.

### Section 5. Authority

The Project manager, and the treasurer at are the sole officers authorized to enter deliver any instrument or payments in the corporation for the implementation of the the direction of the Project Manager into any contract or execute and name of, and on behalf of the Project.

### Section 6. Limitations of Authority.

The Project Manager may make no agreements or contract resulting either in spending beyond that approved in the Project budget or resulting in contractual obligations which are outside the scope of the project without Board consent and written approval.

## Article 7

## Execution of Instruments, Deposits and Funds

### Section 1. Execution of Instruments

The Board of Directors, except as otherwise provided in these Bylaws, may by resolution authorize any officer or agent of the corporation to enter into any contract or execute and deliver any instrument in the name of and on behalf of the corporation, and such authority may be general or confined to specific instances. Unless so authorized, no officer, agent or employee shall have any power or authority to bind the corporation by any contract or engagement or to pledge its credit or to render it liable monetarily for any purpose or in any amount.

### Section 2. Checks and Notes

Except as otherwise specifically determined by resolution of the Board of Directors, or as otherwise required by law, checks, drafts, promissory notes, orders for the payment of money and other evidence of indebtedness of the corporation shall be signed by the Treasurer of the corporation or the ConChair.

The ConChair may only sign cheques which cover items related to their convention and which are part of the approved budget.

### Section 3. Deposits

All funds of the corporation shall be deposited from time to time to the credit of the corporation in such banks, trust companies or other depositories as the Board of Directors may select.

### Section 4. Gifts

The Board of Directors may accept on behalf of the corporation any contribution, gift, bequest or devise for the nonprofit purposes of this corporation.

## Article 8

## Corporate Records, Reports and Seal

### Section 1. Maintenance of Corporate Records

The corporation shall keep at its principal office:

- a. Minutes of all meetings of directors, committees of the board, indicating the time and place of holding such meetings, whether regular or special, how called, the notice given and the names of those present and the proceedings thereof;

- b. Adequate and correct books and records of account, including accounts of its properties and business transactions and accounts of its assets,
liabilities, receipts, disbursements, gains and losses;

- c. A copy of the corporation's Articles of Incorporation and Bylaws as amended to date will be placed on the corporate web site for public inspection and review.

### Section 2. Directors' Inspection Rights

Every director shall have the absolute right at any reasonable time to inspect and copy all books, records and documents of every kind and to inspect the physical properties of the corporation and shall have such other rights to inspect the books, records and properties of this corporation as may be required under the Articles of Incorporation, other provisions of these Bylaws and provisions of law.

### Section 4. Right To Copy And Make Extracts

Any inspection under the provisions of this Article may be made in person or by agent or attorney and the right to inspection shall include the right to copy and make extracts.

###Section 5. Periodic Report

The board shall cause any annual or periodic report required under law to be prepared and delivered to an office of this state and made available online, to be so prepared and delivered within the time limits set by law.

## Article 9

## Lesser Officers

### Section 1.

#### Information Technology Officer

The Information Technology (IT) Officer shall be selected by the Board of
Directors and must meet the same qualifications as a Permanent Officer, as
outlined in Article 4, section 2. The IT Officer shall have responsibilities as
follow:

- a. ensure the availability of the corporate Internet presence is maintained

- b. maintain a list of administrator accounts and passwords for all corporate Internet tools and keep on file with Board Secretary.

- c. ensure that the appropriate accounts and passwords are changed at the start of each new convention year, and that the new administrative information is passed on to the new ConChair's designee(s)

- d. provide research and recommendations to the Board of Directors and ConChair(s) regarding Internet tools and services and potential new Internet tools and services

- e. maintain a web site for the Board of Directors onto which the IT officer shall ensure documents required by the Board be placed

- f. coordinate with current and future ConChairs to ensure that whenever possible, IT decisions are made with long-term benefits in mind

- g. report to the Board of Directors if any decisions will have effects that last beyond the tenure of the currently elected ConChairs

- h. attend meetings of the Board of Directors

- i. maintain a working knowledge of the general function of the convention, its current tools and methods, and its IT needs so as to better be able to make recommendations

- j. ensure that backups of all corporate IT assets are maintained and restores are tested regularly.

## Article 10

## Amendment of Bylaws

### Section 1. Amendment

Subject to limitations specified under provisions of law, these Bylaws, or any of them, may be altered, amended or repealed and new Bylaws adopted by approval of the majority of the full Board of Directors.

## Article 11

## Construction and Terms

If there is any conflict between the provisions of these Bylaws and the Articles of Incorporation of this corporation, the provisions of the Articles of Incorporation shall govern.

Should any of the provisions or portions of these Bylaws be held unenforceable or invalid for any reason, the remaining provisions and portions of these Bylaws shall be unaffected by such holding.

All references in these Bylaws to the Articles of Incorporation shall be to the Articles of Incorporation, Articles of Organization, Certificate of Incorporation, Organizational Charter, Corporate Charter or other founding document of this corporation filed with an office of this state and used to establish the legal existence of this corporation.

All references in these Bylaws to a section or sections of the Internal Revenue Code shall be to such sections of the Internal Revenue Code of 1986 as amended from time to time, or to corresponding provisions of any future federal tax
code.

## Article 12

## Exclusions

### Section 1.

As celebrity guests are difficult to obtain with a notice of less than a year, the board may confirm guests of honor for a particular convention, providing that this confirmation occurs in advance of the appointment of the ConChair for that particular convention.

ADOPTION OF BYLAWS

We, the undersigned, are all of the initial directors or incorporators of this corporation, and we consent to, and hereby do, adopt the foregoing Bylaws, consisting of __________ preceding pages, as the Bylaws of this corporation.

Dated: __________


